from sh1106 import SH1106_I2C


class Display:
    def __init__(self, i2c):
        self.charge = 0
        self.threshhold_current = 0.2

        self.roller = "|/-\\|/-\\"
        self.roller_id = 0
        
        self.width=128
        self.height=64
        self.i2c_address=0x3c
        self.display = SH1106_I2C(self.width, self.height, i2c, None, self.i2c_address, rotate=180, delay=0)
        self.display.init_display()

    def show(self, data):
        self.display.fill(0)
        self.show_state(data)
        self.show_vi(data)
        self.display.hline(0, 26, self.width, 1)
        self.show_time_left(data)
        self.show_progress_bar(data)
        self.show_roller()
        self.display.show()

    def show_roller(self):
        self.display.text(self.roller[self.roller_id], 116, 2, 0)
        if len(self.roller) > self.roller_id + 1:
            self.roller_id = self.roller_id + 1
        else:
            self.roller_id = 0

    def show_state(self, data):
        self.display.fill_rect(0, 0, self.width, 11, 1)

        if data["direction"] == 'charging':
            self.display.text('CHARGING', 32, 2, 0)
        elif data["direction"] == 'discharging':
            self.display.text('DISCHARGING', 20, 2, 0)
        else:    
            self.display.text('HOLD', 48, 2, 0)

    def show_vi(self, data):
        current = abs(data["current"])
        voltage = data["voltage"]
        self.display.text(f'I: {current:2.0f}A, U: {voltage:.1f}V', 0, 15)

    def show_time_left(self, data):       
        time_hours_left = data["time_left"]

        if time_hours_left < 10:
            time_left = self.float_to_hhmmss(time_hours_left)
        else:
            time_left = '  -'

        self.display.text(f'Time left: {time_left}', 0, 31)

    def show_progress_bar(self, data):
        charge = data["charge"]
        
        self.display.rect(0, 42, self.width, 22, 1)
        self.display.fill_rect(0, 42, int(128*charge/100), 22, 1)

        if charge == 100:
            self.display.fill_rect(48, 47, 36, 11, 0)
        else:
            self.display.fill_rect(56, 47, 28, 11, 0)

        self.display.text(f'{charge:3d}%', 50, 49)
        
    def float_to_hhmmss(self, hours):
        hh = int(hours)
        
        minutes = (hours - hh) * 60
        mm = int(minutes)
        
        return f"{hh:d}h{mm:02d}m"
