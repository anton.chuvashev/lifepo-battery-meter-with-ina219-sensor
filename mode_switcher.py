class ModeSwitcher:
    def __init__(self, modes):
        self.modes = modes + [modes[0]]

    def get_mode(self):
        with open("mode", "r") as f:
            return f.readline().strip()
        
    def set_mode(self, mode):
        with open("mode", "w") as f:
            f.write(mode)
        
    def set_next_mode(self):    
        mode = self.get_mode()
        new_mode = self.modes[self.modes.index(mode) + 1]
        self.set_mode(new_mode)