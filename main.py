from machine import Pin
from time import sleep, time
import gc
#from neopixel import NeoPixel

from mode_switcher import ModeSwitcher


#pin = Pin(2, Pin.IN)

hw_config = {
    "time": time(),
    "mode_switcher": ModeSwitcher(["setup","service"]),
#    "np": NeoPixel(Pin(0, Pin.OUT), 1),
}

def irq_callback(p):
    global hw_config

#    hw_config["np"] = NeoPixel(Pin(0, Pin.OUT), 1)
    
    if hw_config["time"] < time() - 2:
        hw_config["time"] = time()
        
#        hw_config["np"][0] = (128, 0, 0)
#        hw_config["np"].write()
            
        hw_config["mode_switcher"].set_next_mode()
        sleep(0.5)        

        machine.reset()


#pin.irq(trigger=Pin.IRQ_RISING, handler=irq_callback)

mode = hw_config["mode_switcher"].get_mode()

if mode == "setup":
    from config import Config
    cfg = Config(hw_config)
    cfg.configure()
else:   
    from service import Service
    srv = Service(hw_config)
    srv.serve()
