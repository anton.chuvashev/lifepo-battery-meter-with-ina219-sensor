from machine import Pin, reset
from time import sleep, time
import network
import socket
import re
import gc

from micropyserver import MicroPyServer

from file_reader import read_config, read_file
from form_data import parse_form_data, generate_form
from pretty_json import format_json


on = True
config = read_config()


def route_css(headers, body):
    return 'HTTP/1.0 200 OK\nContent-Type: text/css\n\n' + read_file("setup.css")


def route_root(headers, body):
    global config
    
    return "HTTP/1.0 200 OK\nContent-Type: text/html\n\n" + read_file("setup.html").format(form_html=generate_form(config))


def route_root_submit(headers, body):  
    global config

    config = parse_form_data(body)
    print("New config:")
    print(config)

    del headers
    del body
    gc.collect()

    new_config_json = format_json(config)

    with open("config.json", "w") as f:
        f.write(new_config_json)

    del new_config_json
    gc.collect()
        
    return "HTTP/1.0 200 OK\nContent-Type: text/html\n\n" + read_file("setup.html").format(form_html=generate_form(config))


class Config:
    def __init__(self, config_param):
        self.hw_config = config_param
        
    def connect_wifi(self):
        global config
        
        cfg = config["Setup"]
        ap = network.WLAN(network.AP_IF)

        ap.active(True)
        ap.config(essid=cfg["ssid"], password=str(cfg["password"]))

        while ap.active() == False:
            pass

        print('WiFi is up')
        print(ap.ifconfig())


    def restart(self):
      print('Restarting...')
      sleep(1)
      reset()
      
    def start_http(self):
        server = MicroPyServer()
        
        server.add_route("/", route_root)
        server.add_route("/", route_root_submit, "POST")
        server.add_route("/setup.css", route_css)

        self.hw_config["np"][0] = (128, 0, 64)
        self.hw_config["np"].write()

        server.start()        
      
    def configure(self):
        try:
            self.connect_wifi()
            self.start_http()
        except OSError as e:
            print(e)
            self.restart()
