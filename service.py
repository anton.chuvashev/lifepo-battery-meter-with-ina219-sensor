from machine import Pin, I2C, reset
from ina219 import INA219
from time import sleep, time
import ubinascii
import network
import socket
import pretty_json
import time
from umqttsimple import MQTTClient
from averagerator import Avg

from file_reader import read_config
from display import Display


class Service:
    def __init__(self, hw_config):
        self.hw_config = hw_config
        self.config = read_config()
        self.mqtt_config = self.config["MQTT"]        
        self.battery_config = self.config["Battery"]
        self.shunt_config = self.config["Shunt"]
        self.ina_config = self.config["Ina"]

        i2c = I2C(Pin(2), Pin(0))
        self.ina = INA219(self.ina_config["shunt_resistance"], i2c)
        self.ina.configure()
               
        self.current_coef = self.get_current_coef()
        
        self.display = Display(i2c)
        
        self.avg = Avg()
        
        self.data = self.get_init_data()
        
        self.soh_data = self.get_init_soh_data()
        
        self.screen_refresh_time = self.get_time()
        self.publish_time = self.get_time()

    def get_init_data(self):
        return {
            "voltage": 0,
            "current": 0,
            "power": 0,
            "charge": 100,
            "led_color": "#FF00FF",
            "time_left": 0,
            "direction": "hold",
            "soh": 0,
        }

    def get_init_soh_data(self):
        return {
            "update_time": 0,
            "charge_time": 0,
            "discharge_time": 0,
            "start_charge": 0,
            "discharge_energy": 0,
        }

    def wait_with_display(self, delay):
        for i in range(delay * 2):
            self.read_sensor()
            self.refresh_screen()
            sleep(0.5)

    def connect_wifi(self):
        self.wait_with_display(2)
        
        cfg = self.config["Service"]
        wlan = network.WLAN(network.STA_IF)

        wlan.active(True)
        wlan.connect(cfg["ssid"], str(cfg["password"]))

        attempts_left = 5

        while not wlan.isconnected():
            if attempts_left == 0:
                print("WIFI connection failed, restarting...")
                self.restart()

            attempts_left -= 1

            print('Waiting for connection...')
            self.wait_with_display(1)

        print('WiFi connected')
        print(wlan.ifconfig())

    def connect_mqtt(self):
        cfg = self.config["MQTT"]

        self.client = MQTTClient(cfg["client_id"], cfg["server"], cfg["port"], cfg["user"], cfg["password"], cfg["keepalive"])
        self.client.connect()

        print("Connected to MQTT broker")

    def restart(self):
      reset()
      
    def serve(self):
        try:
            self.connect_wifi()
            self.connect_mqtt()
        except OSError as e:
            print("Service loop OSError, restarting...")
            self.restart()

        while True:
            try:               
                self.read_sensor()             
                self.publish_mqtt()
                self.refresh_screen()
            except OSError as e:
                self.restart()

    def publish_mqtt(self):
        if self.is_time_to_publish():           
            json_text = pretty_json.format_json(self.data)
            self.client.publish(self.mqtt_config["topic"], json_text)
        
    def refresh_screen(self):
        if self.is_time_to_refresh_screen():
            self.display.show(self.data)

    def is_time_to_publish(self):
        interval = self.mqtt_config["interval"] * 1000
        
        if self.get_time() > self.publish_time + interval:
            self.publish_time = self.get_time()
            return 1
        
        return 0

    def is_time_to_refresh_screen(self):
        interval = self.config["Service"]["screen_refresh_interval"] * 1000
        
        if self.get_time() > self.screen_refresh_time + interval:
            self.screen_refresh_time = self.get_time()
            return 1
        
        return 0

    def get_time(self):
        return int("{:.20d}".format(time.time_ns())[:-6])

    def get_current_coef(self):
        ina_config = self.ina_config
        # Measured by ina shunt voltage is lower because of internal ina shunt resistor
        # which is connected in parallel with main shunt
        ina219_resistance_coef = ina_config["resistance_coef"]

        # Orig: 320mV = 3.2A -> 100mV / A
        # Shunt: 75mV = 100A -> 0.75mV / A
        ina219_shunt_voltage = ina_config["max_shunt_voltage"]
        ina219_shunt_current = ina_config["max_shunt_current"]

        # 1000 is for conversion mA -> A
        ina219_scale = ina219_shunt_voltage / ina219_shunt_current / 1000
        shunt_scale = self.shunt_config["max_voltage"] / self.shunt_config["max_current"]

        current_coef = ina219_scale / shunt_scale * ina219_resistance_coef
        
        return current_coef

    def read_sensor(self):
        count = 100         
        voltage_sum = 0
        current_sum = 0

        for i in range (count):
            voltage_sum += self.ina.voltage()
            current_sum += self.ina.current()

        voltage = voltage_sum / count
        current = current_sum / count * self.current_coef
        power   = voltage * current

        charge = self.get_charge(voltage, current)
        capacity_wh = self.battery_config["capacity"] * self.battery_config["voltage"]
        threshhold_power = 5
        old_direction = self.data["direction"]
        
        self.data["voltage"] = round(voltage, 2)
        self.data["current"] = round(current, 2)
        self.data["power"] = round(power, 2)
        self.data["charge"] = abs(charge)
        
        if current > 0.2:
            self.data["direction"] = "discharging"
            capacity_left = capacity_wh * self.data["charge"] / 100
        elif current < -0.2:            
            self.data["direction"] = "charging"
            capacity_left = capacity_wh * (100 - self.data["charge"]) / 100
        else:
            self.data["direction"] = "hold"
            capacity_left = 0
       
        if abs(self.data["power"]) > threshhold_power:
            self.data["time_left"] = abs(capacity_left / self.data["power"])
        else:
            self.data["time_left"] = 999

        if voltage > self.battery_config["voltage_status"]["ok"]:
            self.data["led_color"] = "#00FF00"
        elif voltage > self.battery_config["voltage_status"]["warn"]:
            self.data["led_color"] = "#FFAA00"
        else:                
            self.data["led_color"] = "#FF0000"

        self.update_soh(old_direction)
        
    def update_soh(self, old_direction):
        if self.data["direction"] != "discharging":
            self.soh_data = self.get_init_soh_data()
            self.data["soh"] = 0
            return
        
        current_time = self.get_time() / 1000
        delta_time = current_time - self.soh_data["update_time"]
        self.soh_data["update_time"] = current_time
        
        if delta_time <= 0:
            delta_time = current_time
               
        self.soh_data["discharge_time"] += delta_time

        # TODO: put to config
        # 180=3min delay for measure
        # 1800=30min delay for calculating
        if self.soh_data["discharge_time"] < 10:
            self.soh_data["start_charge"] = self.data["charge"]
            self.soh_data["discharge_energy"] = 0
        else:
            self.soh_data["discharge_energy"] += self.data["current"] * delta_time / 3600

            delta_charge = self.soh_data["start_charge"] - self.data["charge"]
            
            if delta_charge > 0:
                delta_energy = self.soh_data["discharge_energy"]
                current_capacity = delta_energy / delta_charge

                self.data["soh"] = int(current_capacity / self.battery_config["capacity"] * 100)

    def get_charge(self, voltage, current):       
        if current > 0:
            adjusted_voltage = voltage + current * self.battery_config["resistance"]
            voltage_charge_map = self.battery_config["voltage_discharge"]
        else:        
            adjusted_voltage = voltage
            voltage_charge_map = self.battery_config["voltage_charge"]

        map_key = max([key for key in voltage_charge_map.keys() if key < adjusted_voltage] + [0])
        
        return int(self.avg.get_value("charge", voltage_charge_map[map_key]))


    def hex_to_rgb(self, hex_color):   
        cfg = self.config["Service"]

        hex_color = hex_color.lstrip('#')
        
        rgb_tuple = tuple(int(cfg["led_brightness"] * int(hex_color[i:i+2], 16)) for i in (0, 2, 4))
                
        return rgb_tuple
