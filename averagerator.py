class Avg:
    def __init__(self):
        self.keys={}

    def get_value(self, key, value, size=10):
        self.keys.setdefault(key, (value,) * size)
        
        self.keys[key] = self.keys[key][1:] + (value,)
        
        return sum(self.keys[key]) / len(self.keys[key])