import json

def format_json(data, indent=0):
    if type(data) is dict:
        result = "{\n"
        keys = sorted(list(data.keys()))

        if len(keys) > 0:
            for key in keys[:-1]:
                value = data[key]
                result += " " * (indent + 2) + f'"{key}": {format_json(value, indent + 2)},\n'

            result += " " * (indent + 2) + f'"{keys[-1]}": {format_json(data[keys[-1]], indent + 2)}\n'

        result += " " * indent + "}"
    elif type(data) is list:
        result = "[\n"
        
        if len(data) > 0:
            for item in data[:-1]:
                result += " " * (indent + 2) + f"{self.format_json(item, indent + 2)},\n"
                
            result += " " * (indent + 2) + f"{self.format_json(data[-1], indent + 2)}\n"
        
        result += " " * indent + "]"
    else:
        result = json.dumps(data)
    
    return result
